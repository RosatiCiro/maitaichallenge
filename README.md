Tap2Flip

Technologies used: SwiftUI, WatchKit

The app is a simple stand-alone game for Apple Watch that, using the flipcards, aims to help the user learn Spanish.

During this challenge, I worked as a front-end developer, creating all the functions that allows the cards to flip.

