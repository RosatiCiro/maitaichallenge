//
//  HostingController.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 15/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<Card> {
    override var body: Card {
        return Card(card: CardModel.previewCard1)
    }
}
