//
//  NotificationController.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 15/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import WatchKit
import SwiftUI
import UserNotifications

class NotificationController: WKUserNotificationHostingController<NotificationView> {

    var card: CardModel!
    
    override var body: NotificationView {
        NotificationView(card: CardModel.previewCard1)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    override func didReceive(_ notification: UNNotification) {
        
        guard (notification.request.content.userInfo["card"] ?? [:]) is [String: String] else {
            return
        }

        // Create a Card from the information in the notification.
        card = CardModel(text: "BED", image: "bed", translate: "Cama")

        notificationActions = [
            UNNotificationAction(identifier: "correct", title: "Correct", options: []),
            UNNotificationAction(identifier: "incorrect", title: "Incorrect", options: [])
        ]
    }
}
