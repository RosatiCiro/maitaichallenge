//
//  QuizView.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 19/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import SwiftUI

struct QuizView: View {
    @State var counter = 0
    @State private var didTap:Bool = false
    
    private var carte = [
        CardModel.previewCard1,
        CardModel.previewCard2
    ]
    
    private var indici : [Int] = CardModel.rand()
    var body: some View {
        
        VStack(spacing: 3.0) {
            
            // 1. Image
            Image("\(carte[indici.randomElement()!].image)")
                .resizable()
                .frame(width: 70.0, height: 70.0)
                .aspectRatio(contentMode: .fit)
                .padding(.top)
            
            // 2. Button
            NavigationLink(destination: ProgressView()){
                Text("\(carte[indici[0]].translate)")
                    .foregroundColor(Color.white)
            }.navigationBarTitle("QuizGame")
                .background(didTap ? Color(red: 10 / 255, green: 10 / 255, blue: 10 / 255) : Color(red: 10 / 255, green: 10 / 255, blue: 10 / 255))
                .cornerRadius(.infinity)
                .simultaneousGesture(TapGesture().onEnded{
                self.didTap = true
            })
            
            // 3. Button
            NavigationLink(destination: ProgressView()){
                Text("\(carte[indici[1]].translate)")
                .foregroundColor(Color.white)
            }.navigationBarTitle("QuizGame")
                .background(didTap ? Color(red: 10 / 255, green: 10 / 255, blue: 10 / 255) : Color(red: 10 / 255, green: 10 / 255, blue: 10 / 255))
                .cornerRadius(.infinity)
                .simultaneousGesture(TapGesture().onEnded{
                self.didTap = true
            })
        }
        .padding(.leading, 10)
        .padding(.trailing, 10)
        .padding(.bottom, 10)
        .padding(.top, 10)
        .foregroundColor(Color.black)
        .font(Font.system(size: 19.0, design: .rounded))
        
    }
}

struct QuizView_Previews: PreviewProvider {
    static var previews: some View {
        QuizView()
    }
}

